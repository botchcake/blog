---
categories:
  - facets
date: 2016-04-23 10:30:00
title: Third facets alpha released.
url: /2016/05/23/facets-alpha-3/
---


Today we tagged a new round of alpha releases for the search ecosystem.
A new alpha was tagged for [Search API](https://www.drupal.org/project/search_api/releases/8.x-1.0-alpha15).
[Facets](https://www.drupal.org/project/facets/releases/8.x-1.0-alpha3) has also seen a new release, that supports Search API alpha-15.
At [Drupalcamp Granada](http://2016.drupalcamp.es/), we released the first alpha for search api sorts, this release now passes tests and no longer needs the [display plugin patch](https://www.drupal.org/node/2648260).

We've seen a whole lot of additional functionality and bugfixes since the previous alpha.
``git checkout 8.x-1.0-alpha3; git diff --shortstat 8.x-1.0-alpha2`` tells us that we changed 70 files, added 2521 lines and removed 2882.
We were able to remove so much code because of the [result sorting fix](http://cgit.drupalcode.org/facets/commit/?id=5823b2dde7321aecb0134b8a7c62a1f9967264a9), [a test cleanup](http://cgit.drupalcode.org/facets/commit/?id=ba4f21ec0d103f2aa59b7a2e94deb88e5d68ae78) and the refactor of the [dropdown](http://cgit.drupalcode.org/facets/commit/?id=77ea6758f873a6facca800747df9acbc995890ac) and [checkbox](http://cgit.drupalcode.org/facets/commit/?id=832ba3a5c4d0069476f463461e0c10565522e022) widgets.

*Changes since 8.x-1.0-alpha2:*
<ul>
  <li><a href="https://drupal.org/node/2724171">#2724171</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>, <a href="https://drupal.org/u/borisson_">borisson_</a>: Create a new processor stage 'Sort'</li>
  <li><a href="https://drupal.org/node/2722267">#2722267</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Fix Facet::getProcessorsByStage</li>
  <li><a href="https://drupal.org/node/2730571">#2730571</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Change description in configuration menu</li>
  <li><a href="https://drupal.org/node/2726455">#2726455</a> by <a href="https://drupal.org/u/ademarco">ademarco</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: QueryString URL processor breaks pagination for views displayed on the same page</li>
  <li><a href="https://drupal.org/node/2712557">#2712557</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>: Language prefix breaks facets</li>
  <li><a href="https://drupal.org/node/2712515">#2712515</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Enable default sorts for new facets</li>
  <li><a href="https://drupal.org/node/2724381">#2724381</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Port "Show more"/"Show less" functionality from D7</li>
  <li><a href="https://drupal.org/node/2724381">#2724381</a> by <a href="https://drupal.org/u/claudiu.cristea">claudiu.cristea</a>: Port "Show more"/"Show less" functionality from D7</li>
  <li><a href="https://drupal.org/node/2725243">#2725243</a>: Duplicate trim for same result filter</li>
  <li><a href="https://drupal.org/node/2713875">#2713875</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/claudiu.cristea">claudiu.cristea</a>: Followup, change method visibility + file newline fix.</li>
  <li><a href="https://drupal.org/node/2711697">#2711697</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>, <a href="https://drupal.org/u/claudiu.cristea">claudiu.cristea</a>, <a href="https://drupal.org/u/marthinal">marthinal</a>: Facet calculations are in the wrong order</li>
  <li><a href="https://drupal.org/node/2713875">#2713875</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>, <a href="https://drupal.org/u/stijn26">stijn26</a>: Use GET request for dropdown widget</li>
  <li><a href="https://drupal.org/node/2723405">#2723405</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: FacetSourceEditForm doesn't redirect to the main list or show a confirmation message</li>
  <li><a href="https://drupal.org/node/2656668">#2656668</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>, <a href="https://drupal.org/u/evaldas-u?kuras">Evaldas Užkuras</a>, <a href="https://drupal.org/u/claudiu.cristea">claudiu.cristea</a>: Facet's results sorting with multiple sorts</li>
  <li>Code style fix.</li>
  <li><a href="https://drupal.org/node/2717903">#2717903</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>: Cleanup for tests</li>
  <li><a href="https://drupal.org/node/2714679">#2714679</a> by <a href="https://drupal.org/u/ggh">GGH</a>, <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Replacing i.e. and e.g. with English words</li>
  <li><a href="https://drupal.org/node/2671696">#2671696</a> by <a href="https://drupal.org/u/slasher13">slasher13</a>, <a href="https://drupal.org/u/borisson_">borisson_</a>: Add composer.json to module</li>
  <li><a href="https://drupal.org/node/2658678">#2658678</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>, <a href="https://drupal.org/u/evaldas-u?kuras">Evaldas Užkuras</a>: Checkbox widget does not enable multiple facet's values at a time</li>
  <li><a href="https://drupal.org/node/2712511">#2712511</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Change admin controller routing for better site builder UX</li>
  <li>Indentation fix</li>
  <li><a href="https://drupal.org/node/2713137">#2713137</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Links widget should provide themable items</li>
  <li><a href="https://drupal.org/node/2712519">#2712519</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Change the @group annotation for core search tests</li>
  <li><a href="https://drupal.org/node/2712505">#2712505</a> by <a href="https://drupal.org/u/strykaizer">StryKaizer</a>: Clean up facet add form</li>
  <li>style fix.</li>
  <li><a href="https://drupal.org/node/2710189">#2710189</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>: Simplify the LinksWidget</li>
  <li><a href="https://drupal.org/node/2709515">#2709515</a> by <a href="https://drupal.org/u/bc">bc</a>, <a href="https://drupal.org/u/borisson_">borisson_</a>: Display labels for entity base fields as well as field.module fields</li>
  <li><a href="https://drupal.org/node/2711205">#2711205</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>: Enable Post Query processors</li>
  <li>Do not call parent render function on facet source listing</li>
  <li><a href="https://drupal.org/node/2696979">#2696979</a> by <a href="https://drupal.org/u/borisson_">borisson_</a>, <a href="https://drupal.org/u/una_maria">una_maria</a>, <a href="https://drupal.org/u/dasjo">dasjo</a>: Active items should have an active class</li>
  <li><a href="https://drupal.org/node/2693861">#2693861</a> by <a href="https://drupal.org/u/marthinal">marthinal</a>: Fix core search tests with postgres</li>
  <li><a href="https://drupal.org/node/2704669">#2704669</a> by <a href="https://drupal.org/u/karlosvh">karlosvh</a>: Remove page parameter from urls</li>
  <li><a href="https://drupal.org/node/2704645">#2704645</a> by <a href="https://drupal.org/u/karlosvh">karlosvh</a>: Multiple facets - links are not correct</li>
  <li><a href="https://drupal.org/node/2704855">#2704855</a> by <a href="https://drupal.org/u/webflo">webflo</a>, <a href="https://drupal.org/u/borisson_">borisson_</a>: Remove diacritics when sorting by display value</li>
</ul>

## What's still to do before beta.

We still have some things to do before we can go into beta and eventually to a stable release for facets.
There's still a bunch of [open issues](https://www.drupal.org/project/issues/search/facets?project_issue_followers=&status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=4&status%5B%5D=16&issue_tags_op=%3D) we need to address.
We'd also very much appreciate manual testing and good issue reports so we can make the next releases better.
The facets module will stay in alpha as long as Search API doesn't release a beta version, so help on the [remaining beta blockers](https://www.drupal.org/project/issues/search/search_api?project_issue_followers=&status%5B%5D=Open&issue_tags_op=%3D&issue_tags=beta+blocker) is very welcome.

If you want to talk to any of us about Facets or another part of the search ecosystem, you can find us on [#drupal-search-api on Freenode](irc://freenode/drupal-search-api).