---
categories:
  - work
date: 2016-06-12 10:30:00
title: Back to work
url: /2016/06/12/back-to-work/
---


Last year, [I quit my job and announced that I'd spend more time working on open source projects](http://tech.dichtlog.nl/work/2015/07/05/leaving-intracto.html).
I set aside 5000 euro for living and traveling and decided I wouldn't go back to work before that money was gone.
Now that is almost the case, it's time to look back at what I did in the past 11 months.

## The setup

On a personal level, this experience has thought me so much about myself.
I planned on going to as much Drupal events / holidays as I could. I didn't.
I actually spent a lot of time at home, much more than expected.
Not just because I figured out very quickly that I don't feel comfortable traveling on my own - though that had a lot to do with it.
The first couple of months were awesome.
I spent a lot of time reading up on core issues and contributing the odd patch.
I focussed heavily on cacheability and wrote [about the lazy builder](http://tech.dichtlog.nl/php/2015/08/03/lazy-builder-callback.html) callback system.
I went to [Drupalaton](http://2015.drupalaton.hu/) with some ex-colleagues, everything was great.

When I went to Spain for [Drupalcon Barcelona](https://events.drupal.org/barcelona2015) however, I noticed a change.
I flew into Valencia and spent a couple of days there, then a couple of days in Tarragona before taking the train to Barcelona.
Those days, I spent alone.
I actually met up with an old school friend who happened to be in Valencia at the same time because I noticed I hadn't talked to anyone at all that day.
While both Valencia and Tarragona were gorgeous, I wasn't happy at all.
I spent so much time on my own and had no one to share my experiences with.

When I got to Barcelona, the first thing I did after checking in to my Airbnb was going to the extended sprints location and talk to people.
It made me feel included, and I got to reconnect with people I hadn't seen in a while.
I picked up some of the issues I'd worked on in the weeks before and even got some useful work done.

It was a great feeling of accomplishment and I figured that I'd finally found why I had taken the leap into full-time contribution.
However, when the actual con started I didn't get to see a lot of those people again.
Some had meetings, others were focussed on other issues and the huge amount of people in the sprint room was overwhelming.
I was so relieved when my roommate [Jimmy](https://twitter.com/StryKaizer/) arrived in Barcelona, finally I didn't feel as alone.

Over the rest of the week, I spent some time with some of the smartest people in the community and had dinner and beers with a bunch of kind and wonderful friends and strangers.

I ended up being drafted by [Nick](https://twitter.com/nick_vh) to work on the Drupal 8 port of facets, on the last day of the con and went on to help build facets trough all the milestones we had set out, I even got to be part of Acquia's module acceleration program because of that.

## The problem

I did a bunch of contributions to the Drupal project, and mainly focussed on the search ecosystem after Drupalcon Barcelona.
But I have to admit, over the last months, I didn't work as hard as I could have.
I actually spent a lot of my time feeling sad.

## The reason

I was in a bad place emotionally, I had a hard time moving on from a breakup that happened years ago and all that time spent alone made me realize that I hadn't taken the time to deal with those feelings.
They say time heals all wounds, and it does, but you can't just wait for wounds to heal.
You need to actually feel the feels before the moving on can start.

I don't like being alone.
I crave approval from other humans and even though people generally are amazed when they find out I've been working for free on the Drupal project, that connection is usually very short and ends up with a free beer in my hands.
Now, don't get me wrong, free beer is the best beer and I love talking to people at events.

But the lasting relationships that are formed from working next to other people is something that I miss, creating something valuable together with people that have a similar commitment to the product you're working on is an amazing feeling.
And when commitment is a part of the reason why you enjoy working with others, it's hard to find that in the open source community.
It'd be absurd to expect other people to spend as much time on a small open source project such as facets.
I can't expect anyone else to spend between 30+ hours a week on anything, as none of us are getting paid to do it.

## The fix

But there is another option to be more in line with others regarding time commitment, and that's going back to work.
I needed to start looking for a job anyway because the money I had set aside in July 2015 is pretty much gone now.
At [frontend united in Ghent](http://frontendunited.org/), I met up with [Pieter](https://twitter.com/mathysp) - an old school friend that I've known for 12+ years who happens to have ended up in Drupal as well.

I mentioned that I was going to start looking for a new employer soon and that I wanted to find a job in a company where I could use my knowledge that I gained from working on Drupal 8 for the past year.
He told me that the company he works for was (and still is) looking for Drupal developers.

A couple of beers, 2 meetings and 11 days later I now am an employee of [dazzle](https://twitter.com/dazzletheweb).
A place where I can learn how to actually build sites again and use my expertise in writing code for d8 to build products with other humans who have the same commitment to making great things for our customers.

This doesn't mean I've completely given up on contributing, but I will be working full time and only be spending time on core and contrib during evening, weekends and at Drupal events.
For example, I will participating in the Search API sprints at the next dev days, [sign up on the spreadsheet to help out](https://docs.google.com/spreadsheets/d/1h70sYdzufJ002r08I0rsL90SNP_At81LfpI3pTIuZvk/edit#gid=0).

Want to know more about this, or just want to have a chat, let's meet up at [Drupal dev days in Milan](http://milan2016.drupaldays.org/).