---
categories:
  - search-api
date: 2021-02-21 08:30:00
title: My last facets release?
url: /2021/02/18/facet-maintainers/
---


# How I became maintainer of the facets module.

If you've ever seen one of my presentations about Search API/Facets at a Drupal conference, you may have heard (part of) this story before,
but I am going to tell it in as much detail as possible here for the first time.

During the Belgian night at DrupalCon Amsterdam (2014), at the party organized by [Calibrate](https://www.calibrate.be/nl) for their anniversary,
I got to talking with Nick about the Search API module and it's progress towards a Drupal 8 port. 
They had started that work at Drupal Dev Days in Szeged earlier that year and I was at that conference as well but had not contributed yet.

So on the last friday of DrupalCon, during the sprint day, I approached Nick again and he placed me, and 2 co-workers (Frederico & Jeroen) at the Facets table.

We had really vivid discussions with Jurcello about the module's structure and set up the basic framework. 
To this day, that structure still mostly stands (what parts are plugins, what services we use, where we plug into the Search API module).  
We committed/pushed all the things to the module, there was no talk of patches/pull requests,
code reviews happened by walking someone else trough the changed and were just pushed to the repo.
This chaos lasted all day and was super fun to do.

The theme of having beers was a given constant throughout the development process of this module, because the next year, at DrupalCon Barcelona, 
during one of the evening parties I got to talking with Nick again and asked about the progress they made on facets, he mentioned they were working on it at the sprints, 
so the next day I moved  from one of the core sprint tables to the Search API island.
StryKaizer (my roommate for that con - and most other conferences since) joined us at the Search API sprint table as well.
We spent the con working on the structure laid out in Amsterdam and tried to improve on that to make it somewhat reliable.

At the end of that week in Barcelona, we had moved from a sandbox module to an actual module on d.o, and we had given commit rights to a handful of people.
I think the list at that point was this:
- jurcello
- martinhal
- StryKaizer
- Nick\_vh
- drunken\_monkey
- Borisson\_ (_<- this one is me!_)

The next couple of years we kept working on this module, and 2015,2016 and 2017 were years that we attended a lot of conferences.
By this point the Facets and Search API modules mostly consisted of _conference driven development_, with the occasional sprint day thrown in.
As well as a couple of weeks sponsored by [Aqcuia's module acceleration program](http://tech.dichtlog.nl/facets/2016/01/30/module-acceleration-program.html).
Of course, this is not to underestimate the work done by many contributors on sunny afternoons on other days.

One highlight I vividly remember is going to DrupalJam with StryKaizer to sprint on Facets, and being the only two people there at the sprint tables and having the best time.
Another highlight was going to the podcast festival in Ghent, and using that opportunity to tag the beta release in their pub.

<blockquote class="twitter-tweet" data-dnt="true"><p lang="en" dir="ltr">Beta, woo! Thanks everyone. Also, wooo! <a href="https://t.co/RzbfKODHvM">pic.twitter.com/RzbfKODHvM</a></p>&mdash; Joris Vercammen (@Borisson) <a href="https://twitter.com/Borisson/status/936979491204861952?ref_src=twsrc%5Etfw">December 2, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

I also remember spending an entire week in a dark room in Lisbon to fix the last bugs and release our first stable release, 
or that time we won an "open source award" in Vienna, for our work on Search API. Most kudos for that, of course, go to the awesome Thomas Seidl.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We just tagged our first stable release, thanks to everyone who helped us get here! <a href="https://twitter.com/StryKaizer?ref_src=twsrc%5Etfw">@StryKaizer</a> <a href="https://twitter.com/Nick_vh?ref_src=twsrc%5Etfw">@Nick_vh</a> <a href="https://twitter.com/ThomasMSeidl?ref_src=twsrc%5Etfw">@ThomasMSeidl</a> <a href="https://twitter.com/marthinal?ref_src=twsrc%5Etfw">@marthinal</a> <a href="https://twitter.com/jurdevries?ref_src=twsrc%5Etfw">@jurdevries</a> <a href="https://twitter.com/novitsh?ref_src=twsrc%5Etfw">@novitsh</a> <a href="https://twitter.com/mrbaileys?ref_src=twsrc%5Etfw">@mrbaileys</a> <a href="https://t.co/9M3BapeBFK">pic.twitter.com/9M3BapeBFK</a></p>&mdash; Joris Vercammen (@Borisson) <a href="https://twitter.com/Borisson/status/1014919174366298112?ref_src=twsrc%5Etfw">July 5, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Yaaaay! Search API won the Drupal award @ at open source. Happy to be a contributor to the ecosystem. <a href="https://twitter.com/ThomasMSeidl?ref_src=twsrc%5Etfw">@ThomasMSeidl</a> <a href="https://twitter.com/Borisson?ref_src=twsrc%5Etfw">@Borisson</a> <a href="https://t.co/JbUgHEoMGf">pic.twitter.com/JbUgHEoMGf</a></p>&mdash; Jimmy Henderickx (@StryKaizer) <a href="https://twitter.com/StryKaizer/status/912727662958989312?ref_src=twsrc%5Etfw">September 26, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

In 2018 I guided a core conversation and opened [a drupal.org issue](https://www.drupal.org/project/ideas/issues/2999664) about getting Search API into drupal core.
We felt like we needed to do this, partly because I was starting to feel the burden of doing a lot of work on the ecosystem and also because I believe that it is better solution to use than what is currently in Drupal Core.

In June of 2019 I changed jobs to go work at Calibrate, 5 years after first drinking beers on their tab, 
this is a really fun job and I even got some time to work on contrib modules.
But later that year, I got put on a Laravel project and have been doing custom projects with them since.

I feel that I can no longer ask my bosses to give me time to work on Drupal contrib,
when I don't apply the the knowledge I’ve learned in my daily work.
I no longer have an itch to scratch either so I'm not motivated to be working on it in my free time anymore.

## Seeking new (co-)maintainer(s)

I have released [version 1.7 of the facets module](https://www.drupal.org/project/facets/releases/8.x-1.7) last week.
This will probably be the last version I will personally tag, but *you* can help maintain this module.
I hope that from reading this story that you feel the warmth of the drupal community and how people will come and help out your project.

We are looking for someone motivated to help maintain the module, the #search channel on the drupal slack will help you out!
Anyone who is interested to help out can let me know, on slack/twitter or trough another channel.
It would be great if we recognize your name from the issue queue or the slack channel - but even if not, we can discuss how you can help.