---
categories:
  - facets
date: 2016-04-12 10:30:00
title: Second facets alpha released.
url: /2016/04/12/facets-alpha-2/
---


Last week, the search eco-system has seen a new release of alpha versions.
A new alpha was tagged for [Search API](https://www.drupal.org/node/2701895), [Search API Solr](https://www.drupal.org/node/2701909), [Search API Page](https://www.drupal.org/node/2701915) and others.
[Facets](https://www.drupal.org/node/2701921) has also seen a new release, that supports Search API alpha-13.

We've seen a whole lot of extra code since the first alpha release; for bugfixes, new tests and additional functionality.
``git checkout 8.x-1.0-alpha2; git diff --shortstat 8.x-1.0-alpha1`` tells us that we changed 126 files, added 4541 lines and removed 1291.

One thing that I'm extra happy for is that we managed to double the amount of unit tests.
Running ``phpunit -c core modules/facets/`` on alpha1 outputs: ``OK (54 tests, 313 assertions)``, while that same command on alpha2 shows that we now have 124 tests: ``OK (124 tests, 478 assertions)``.
We also tried to be strict in adhering to the drupal coding standards, with 9 commits dedicated to keeping the codebase in shape, resulting in no output from running ``phpcs --standard=Drupal modules/facets``.

Even more awesome is that there's been a number of new people that contributed since alpha 1, by reporting and helping resolve issues.

A couple of highlights of issues that have been included in alpha-2 are:

- [Initial support for a date query type - for core search](http://drupal.org/node/2672024);
- [A new widget: select/dropdown](http://drupal.org/node/2636328);
- [Facet block not immediately visible in 'blocks' after creation](http://drupal.org/node/2661724);
- [Limiting to 1 active item](http://drupal.org/node/2625188);
- [Nofollow on links for search engines](http://drupal.org/node/2183757).

## What's still to do before beta.

We still have some things to do before we can go into beta and eventually to a stable release for facets.
There's still a bunch of [open issues](https://www.drupal.org/project/issues/search/facets?project_issue_followers=&status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=4&status%5B%5D=16&issue_tags_op=%3D) we need to address.
We'd also very much appreciate manual testing and good issue reports so we can make the next releases better.

If you want to talk to any of us about Facets or another part of the search ecosystem, you can find us on [#drupal-search-api on Freenode](irc://freenode/drupal-search-api).