---
categories:
  - Jekyll
date: 2015-07-01 22:30:00
title: Deploying jekyll trough bitbucket with wercker.
url: /2015/07/01/deploying-jekyll-trough-bitbucket-with-wercker/
---


<div class="p-4 bg-red-lighter">
Wercker service will shutdown on October 31st, 2022. So everything mentioned here about that is no longer valid.
</div>

A couple of months ago I ported this blog from a custom PHP application to [Jekyll](http://jekyllrb.com/).
Back then I didn't take the time to set up a deployment workflow.
When I wrote the blogpost about Grunt yesterday, I was annoyed that I had open [FileZilla](https://filezilla-project.org/download.php?type=client) to deploy the site.
So today I wanted to set up an automated build/deployment system for this blog.

Since the sourcecode for this blog is hosted on [Bitbucket](https://bitbucket.org/), I needed to find a system that can run off a webhook and can run custom code in the build and deploy stages.
[Wercker](http://wercker.com/) is platform that does exactly that.

First of all, Wercker runs [Docker](https://www.docker.com/) instances to build software, so the first thing you'd need to do is installing [boot2docker](http://boot2docker.io/) if you're running on osx or windows.

Next create a ``wercker.yml`` file in the root of the web-directory.

```
box: phusion/passenger-ruby22
no-response-timeout: 10
build:
    steps:
    # Run smart version of bundle install
    - bundle-install
    # Run Jekyll doctor
    - script:
        name: Run Jekyll doctor
        code: bundle exec jekyll doctor
    # Build Jekyll public site
    - script:
        name: Build Jekyll site
        code: bundle exec jekyll build --trace
```
This is what the basic definition and build step of the wercker file look like.
There's a ``bundle-install`` to install the dependencies and ``jekyll doctor`` / ``jekyll build`` to actually build the project.
This didn't work for me initially becuase I didn't have a ``Gemfile`` in my repository.
Taking the Gemfile that is the default for github-pages seemed to do the trick.

```
source 'https://rubygems.org'

require 'json'
require 'open-uri'
versions = JSON.parse(open('https://pages.github.com/versions.json').read)

gem 'github-pages', versions['github-pages']
```
So now we can build the jekyll-project on wrecker, now all that's needed is setting up deployment.
Because I already have a simple ftp server set up where this project is already being hosted, I wanted to deploy to that environment.
Most tutorials you find online for deploying with wercker use amazon s3 or aws for hosting.
Setting up deployment for ftp is easy enough though.
I added this in the wrecker.yml and set up `$FTP_USERNAME` and `$FTP_PASS` as environment variables in the wrecker dashboard.

```
deploy:
    steps:
        - install-packages:
            packages: lftp
        - script:
            name: prepare lftp settings
            code: |
              mkdir -p $HOME/.lftp
              echo "set ssl:verify-certificate no" > $HOME/.lftp/rc
        - script:
            name: sync with remote
            code: lftp -e "set ftp:ssl-allow no;mirror -R \_site web/" -u $FTP_USERNAME,$FTP_PASS example.com
```
