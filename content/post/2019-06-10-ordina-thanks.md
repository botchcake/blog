---
categories:
  - search-api
date: 2019-06-11 08:30:00
title: Huge thanks to Ordina for stepping up and contributing back to Drupal & the Search API Ecosystem
url: /2019/06/10/ordina-thanks/
---


At Drupal Europe in Darmstadt last year,
I gave a [presentation about Search API](https://www.drupaleurope.org/session/bringing-search-api-drupal-core) and it's possible path towards core inclusion.
One of the reasons why we want to do this is to improve the overall health of the ecosystem by exposing the internals to more people.
When I gave the talk, 5 People were responsible for ~60% of the code for the entire ecosystem, including Search API, Search API solr, Search API autocomplete, Search API pages, Facets.

There were 3 things that happened as a result of that session.

* We created an [issue on the drupal ideas queue](https://www.drupal.org/project/ideas/issues/2999664) about moving search api into core with 4 possible ways of achieving that goal.
* We opened another idea about [ecosystem landing pages](https://www.drupal.org/project/ideas/issues/2999300), and we kind of got that trough the new ecosystem field.
* We were approached by [Legolasbo](https://drupal.org/u/legolasbo) from [Ordina](https://www.drupal.org/ordina-digital-services) about helping out with the code.

That last point is one I'd like to focus on in this post.

At the end of last year, we checked in with them about this.
They committed to help in the Search API issue queue for 8 hours/week over the first half of 2019, this is *208 hours* in total.
They've almost completed that, so this is a good time to look back.
Since the start of their commitment, they uploaded patches to at least 47 issues spread out over 4 modules.
Of those 47, 40 have been committed already with a couple of others on RTBC.

One of the awesome things that [Legolasbo](https://drupal.org/u/legolasbo) started with, was doing backlog grooming for 6 of the modules, including Search API.
This meant that he closed 750+ old, outdated issues, making the backlog get into a somewhat more managable state.

One of the other things that we discussed in the beginning was that other Ordina people would also be able to contribute.
This was a good way to intruduce more people to drupal's contribution flow and spread the knowledge over even more people.
We additionally saw patches by: [LaravZ](https://drupal.org/u/laravz) and [marcoweijenborg](https://drupal.org/u/marcoweijenborg).

They also helped land some gnarly issues over the last months, and helpful performance improvements, like:
* [#2867479, finalize the removal of taxonomy term handlers sub module](https://drupal.org/i/2867479)
* [#2898327, performance improvements in addResults](https://drupal.org/i/2898327)
* [#3014641, performance improvements on indexing large datasets](https://drupal.org/i/3014641)
* [#3023704, convert hooks to events](https://www.drupal.org/project/search_api/issues/3023704).

After 4 months of getting to know the Search API module, they also branched out into other modules as well.

One of those modules they touched was the Search API pages module.
The Drupal 8 version of that module had not gotten out of Alpha yet. It had a couple of open issues that were not handled yet.
Because of their work, we tagged a first beta release at drupal dev days with the hopes of getting out a stable release in the fall.
The impact on this module has been immense, they touched 17 issues, of which 15 are closed now, leaving only 11 open issues, effectivly closing 61% of issues.

The only thing left for us to do is to give Ordina a big thank you and a hug.