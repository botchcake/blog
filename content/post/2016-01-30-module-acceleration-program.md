---
categories:
  - facets
date: 2016-01-30 18:30:00
title: Module acceleration program
url: /2016/01/30/module-acceleration-program/
---


Over the last 2 months (december 2015 and januari 2016), there's been immense progress to [Search API](https://www.drupal.org/project/search_api) and [Facets](https://www.drupal.org/project/facets) for drupal 8.
[Acquia](http://www.acquia.com) offered to sponsor me and [Thomas Seidl (drunken monkey)](https://www.drupal.org/u/drunken-monkey) to spend some of our time working on the search ecosystem for Drupal 8.
We were part of the [D8 Module Acceleration Program](https://dev.acquia.com/blog/d8-module-acceleration-program--january-releases/27/01/2016/9581).
Thomas also posted some information about the work we did in a [recent blogpost](https://drunkenmonkey.at/blog/search_api_d8_alpha_12).

Most of this work can be seen in the latest [alpha-12](https://www.drupal.org/node/2657540) release for Search API.
We've also seen a compatible alpha-release for [Search API Attachments](https://www.drupal.org/node/2657746).
For Search API Solr, we need to get the [testsuite running again](https://www.drupal.org/node/2595077) before we can create a compatible alpha release, but the current HEAD should work with alpha-12.
Facets is almost ready for it's first alpha release, we first need to [add a basic readme](https://www.drupal.org/node/2606528) file before we're allowed to create a first alpha release.

## The biggest issues we've tackled trough the D8 MAP.

We made sure we're explicit in the [cacheability](https://www.drupal.org/node/2624472) [metadata](http://cgit.drupalcode.org/facets/log/?qt=author&q=Wim+Leers) we're emitting with the help from the awesome [Wim Leers](https://www.drupal.org/u/wim-leers).
We decided that for now, search and facets will not be cacheable, but [big pipe](https://www.drupal.org/project/big_pipe) will be great solution to speed up the rendering of a search page.
See [the comment added in the FacetBlock](http://cgit.drupalcode.org/facets/commit/?id=86dfe25567e7187b79721ee0ac5c2611bad7a979) class for more info about this.

We should also explicitly thank [Xano](https://www.drupal.org/u/xano) for helping with the integration of [Search API](https://www.drupal.org/node/2624860) and [Facets](https://www.drupal.org/node/2616108) with the [Plugin module](https://www.drupal.org/project/plugin).

We've made massive steps in the Views integration for Search API, by [adding Views support for individual fields](http://drupal.org/node/2470914) and [adding Views filters for all indexed fields](http://drupal.org/node/2601224).
These were the biggest hurdles to take for better views integration, that are still in the current [list of Search API beta blockers](https://www.drupal.org/project/issues/search/search_api?status[0]=Open&version[0]=8.x&issue_tags=beta%20blocker&order=field_issue_priority&sort=desc).

We've also put a lot of work in cleaning up some of the code to make for a saner external API, such as the big [index cleanup](http://drupal.org/node/2638116), the smaller [field cleanup](http://drupal.org/node/2645098), and another [index cleanup](https://www.drupal.org/node/2317771).
A [similar cleanup](http://drupal.org/node/2656226) also happened for [the facet entity](http://drupal.org/node/2645128).

[The index's behavior when a dependency is removed](https://www.drupal.org/node/2574633) was also improved.
We're now not removing the index but trying to just remove the plugin.
This should make for a better experience when working with contributed/custom datasources, processors, trackers, or backends.

One of the things we're very proud of for the d8 version of Search API and Facets is the vastly improved test coverage, that makes us more confident in making changes.
We've also improved the test quality of the previous 2 months as well.
We've removed [random strings](https://www.drupal.org/node/2643000) from the tests, to reduce unwanted random failures, as well as moving [away from simpletest to phpunit tests](http://drupal.org/node/2642728) where possible.
As well as adding a couple of [new](http://drupal.org/node/2625264) [testcases](http://drupal.org/node/2473267).

For facets, we've had to make sure that the [testbots picked up all the tests](http://drupal.org/node/2638330), and we added a bunch of [extra](http://drupal.org/node/2613186) [coverage](https://www.drupal.org/node/2640746).
We made sure to add [extra](http://drupal.org/node/2656190) [regression tests](http://drupal.org/node/2656220) when a bug was reported.
We've also enabled [strict config schema checking](http://drupal.org/node/2657382) in the tests as well.

Most of these changes are only in code, we've also did a very big UI/UX change by creating [a Views-like UI for adding/removing fields to an index](http://drupal.org/node/2574969).
Feedback and improvements for that UI should go in [this issue](https://www.drupal.org/node/2641388).
On top of all that, we've added the ability to [filter languages in the entity datasource](https://www.drupal.org/node/2256219) and for [backends to provide information about it's availability](https://www.drupal.org/node/2134799).

We've added [Facet source specific storage](https://www.drupal.org/node/2612090), this enabled us to [make it easier to switch the url processor](http://drupal.org/node/2637594).
The url handling was also improved [by adding settings to change the url alias](http://drupal.org/node/2624410).
At drupalcon Barcelona we decided to use the core context api for Facets' block handling but we stepped away from that and moved to [creating a block per facet](http://drupal.org/node/2639560) recently.

We added a [checkbox widget](http://drupal.org/node/2596337) as well.
Making sure we had 2 widgets was also a good testcase for our widget selection system.
Another big issue was [porting the facet dependencies](http://drupal.org/node/2596447) from drupal 7's [facetapi_bonus](http://drupal.org/project/facetapi_bonus) module, as well as adding [support for and/or selection](https://www.drupal.org/node/2625186).

### We can still use some help.

If you want to help with creating an awesome search ecosystem in any way, please either ask around in the Search API IRC channel ([#drupal-search-api on Freenode](irc://freenode/drupal-search-api)) or jump into one of the issue queues.
We appreciate code as well as help in creating detailed issue reports and manual testing.