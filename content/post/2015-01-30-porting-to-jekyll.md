---
categories:
  - Jekyll
date: 2015-01-30 20:05:55
title: Porting my Blog to Jekyll
url: /2015/01/30/porting-to-jekyll/
---


The blog I previously had was a custom project written in [CodeIgniter](http://www.codeigniter.com/), back in 2012. I needed to have a place to write down blog posts and as we all know, creating a custom cms for your blog is something every developer does.

My writing flow consisted of opening up a [Markdown](http://daringfireball.net/projects/markdown/syntax) editor, usually [Sublime Text](http://www.sublimetext.com/2) on windows or [Mou](http://25.io/mou/) on OSX, writing the blogpost, converting it into html trough the [markdown dingus](http://daringfireball.net/projects/markdown/dingus) and copying that html into the wysiwyg editor on the website.

This workflow is pretty unwieldy and because I'm too lazy to actually update the code of the CodeIgniter blog, it was using a horribly outdated version.
When I sat down today to write a new blog-post, I really wanted to have something that generated html files out of my markdown.
I had heard of [Jekyll](http://jekyllrb.com/), [Hyde](http://hyde.github.io/) and [Sculpin](https://sculpin.io/) before.
After some testing I found that Jekyll was the easiest to get what I want from it.
So now I'm running the blog trough that. Jekyll takes my markdown files and creates a folder that I just have to upload to the webserver and be done with it.
All I had to do the "migrate" the old posts was just grab my old markdown files, put the header-information in them and paste them in the ``_posts`` directory.

Currently the deployment flow is just dropping that folder into filezilla. Of course there are better options, but for now this'll do.