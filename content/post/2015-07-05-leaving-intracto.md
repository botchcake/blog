---
categories:
  - work
date: 2015-07-05 21:15:00
title: Leaving Intracto, Doing more for the community
url: /2015/07/05/leaving-intracto/
---


I've quit the company I've been with since september 2012, [Intracto](http://www.intracto.com).
Intracto has been a really good environment for me but since I've started working with [Drupal](http://drupal.org/) I've grown to appreciate and love open source and the drupal project.
I really believe in the power of open source,
and I want to prove that by doing my part in making the Drupal project better.

So starting July 20 I'll put my money where my mouth is and I'll start working on Drupal core full time.
I've got some cash saved up and I really want to give back to the community and this seems like a good way to do it.

I'm really interested in most parts of the system but I've been doing some work in [cacheability](https://www.drupal.org/project/issues/search/drupal?project_issue_followers=&status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&issue_tags_op=%3D&issue_tags=D8+cacheability) and I'm starting to be a little bit comfortable in that area.

I really want to help everywhere my help is useful so caching isn't my only focus for the upcoming months but it is one area I think I can be useful in.

If you want some help working on issues, ping me on irc (borisson\_) or find [another way of contacting me](/about).

I want to combine this contributing time with doing as much traveling across Europe as possible, so I'll try to attend a couple of conferences as well over the summer, the first one will be [drupalaton](http://2015.drupalaton.hu/) in Hungary this august.