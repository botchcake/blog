---
categories:
  - facets
date: 2016-02-04 19:05:00
title: First facets alpha released.
url: /2016/02/03/facets-first-alpha/
---


In my [previous post](http://tech.dichtlog.nl/facets/2016/01/30/module-acceleration-program.html), I mentioned that we were very close to releasing the [first alpha version of facets](https://www.drupal.org/node/2661708) and yesterday [Nick](https://www.drupal.org/u/nick_vh) did exactly that.
This version of facets is compatible with search_api-alpha12.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">We released alpha1 facets (aka Facet API) for D8! <a href="https://t.co/7DKqRS6Cvj">https://t.co/7DKqRS6Cvj</a> Thanks to <a href="https://twitter.com/Borisson">@Borisson</a> et al! <a href="https://twitter.com/hashtag/drupal8?src=hash">#drupal8</a> <a href="https://twitter.com/hashtag/drupal?src=hash">#drupal</a></p>&mdash; Nick Veenhof (@Nick_vh) <a href="https://twitter.com/Nick_vh/status/694860632580046848">February 3, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

I want to thank everyone who helped out on creating the first alpha.
We've had a total of 421 commits to the 8.x-1.x branch, 90 of which happened in the last 2 months.
The list below is based on the output from ``git checkout 8.x-1.0-alpha1; git shortlog -sn``.

- [Joris Vercammen (borisson\_)](https://drupal.org/u/borisson_)  
- [José Manuel Rodríguez Vélez (marthinal)](https://drupal.org/u/marthinal)
- [Jimmy Henderickx (StryKaizer)](https://drupal.org/u/StryKaizer)
- [Nick Veenhof (nick_vh)](https://www.drupal.org/u/nick_vh)
- [Jur de Vries (jurcello)](https://www.drupal.org/u/jurcello)
- [Ivo Van Geertruyen (mr.baileys)](https://www.drupal.org/u/mr.baileys)
- [Matthias Vandermaesen (netsensei)](https://www.drupal.org/u/netsensei)
- [Mike Keran (mikeker)](https://www.drupal.org/u/mikeker)
- [Wim Leers](https://www.drupal.org/u/wim-leers)
- [Kevin Thiels (novitsh)](https://www.drupal.org/u/novitsh)
- [Frederico Taramaschi (fredericot)](https://www.drupal.org/u/fredericot)
- [Christian Adamski](https://www.drupal.org/u/ChristianAdamski)
- [Evaldas Užkuras](https://www.drupal.org/u/evaldas-u%C5%BEkuras)
- [Mattias Michaux (mollux)](https://www.drupal.org/u/mollux)
- [Kristof De Jaeger (swentel)](https://www.drupal.org/u/swentel)
- [Ed Crompton (eddie_c)](https://www.drupal.org/u/eddie_c)
- [Felipe Ribeiro (felribeiro)](https://www.drupal.org/u/felribeiro)
- [Tom Verhaeghe](https://www.drupal.org/u/tom-verhaeghe)
- [michiellucas](https://www.drupal.org/u/michiellucas)
- [Sorin Dediu (sdstyles)](https://www.drupal.org/u/sdstyles)

## What's still to do before beta.

We still have some things to do before we can go into beta and eventually to a stable release for facets.
There's a bunch of [open issues](https://www.drupal.org/project/issues/search/facets?project_issue_followers=&status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=4&status%5B%5D=16&issue_tags_op=%3D) we still need to finish.
We'd also very much appreciate manual testing and good issue reports so we can make the next releases better.

We currently have 12% test coverage from the [unit tests](http://cgit.drupalcode.org/facets/tree/tests/src/Unit/Plugin?h=8.x-1.0-alpha1) on the ``src/`` directory and getting to 100% is unattainable, but I personally think that we should get better test coverage so that the confidence in the codebase can increase.
We do have a couple of [browser tests](http://cgit.drupalcode.org/facets/tree/src/Tests?h=8.x-1.0-alpha1) as well, including a couple of tests that were introduced together with bug reports so that we don't introduce regressions.
We do already have decent testcoverage so far; but we can always improve that.

If you want to talk to any of us about Facets or another part of the search ecosystem, you can find us on [#drupal-search-api on Freenode](irc://freenode/drupal-search-api).