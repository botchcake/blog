---
categories:
  - PHP
date: 2015-03-22 13:38:55
title: Radio wizi
url: /2015/03/22/radio-wizi/
---


Samen met een aantal collega's hebben we de voorbije weken een bot gemaakt voor [Hipchat](https://www.hipchat.com/).
We hebben eerst een bot gemaakt die op 1 channel luistert naar elke message die gepost wordt.

Op elke message kijken we via een reguliere expressie of dit een link is naar youtube.
``%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i``.
Deze expressie kijkt of dit een van de vele geldige youtube-domeinen is
en of er een Youtube-id in de link zit.

Als we een Youtube-id gevonden hebben kijken we via de Youtube-api of deze video afgespeeld mag worden in België en of deze geëmbed mag worden. Dit is niet voor alle video's toegestaan en daarom kijken we dit dus eerst na.
Van zodra we hebben gevalideerd dat dit een correcte Youtube-link is, geven we zowel de Youtube-link als de naam van de persoon die deze link gepost heeft door aan de radio-app.

<script src="https://gist.github.com/borisson/07138bedce39191ff775.js"></script>

Onze radio-app is een eenvoudige Symfony website die de links die gepost worden nogmaals nakijkt.
Als deze links nagekeken worden, worden deze in een MySQL database opgeslagen.

We hebben hier een frontend bij geschreven gemaakt in angularJS die elke 5 seconden aan de backend vraagt of er nieuwe nummers gepost zijn.
Deze nieuwe nummers voegen we meteen toe aan de queue.

We zorgen er ook voor dat we het start-tijdstip van een nummer meteen opslaan, als persoon X om 13:24:00 begint aan een nummer en persoon Y laad om 13:24:30 de browser, dan wordt voor persoon Y de eerste 30 seconden van het nummer overgeslagen.
Op die manier zorgen voor een authentieke radio-ervaring waarbij iedereen naar hetzelfde nummer aan het luisteren is.

We hebben nog een heel aantal features die we nog kunnen bij bouwen, waaronder een volledig redesign.
Hier wordt af en toe aan gewerkt na de uren en hopelijk komen die er nog bij.
Je kan alvast eens kijken op de [publieke Github repository](https://github.com/borisson/hipchat-youtube-queue).

Ik wil zeker en vast al mijn collega's [Griffin](https://twitter.com/GriffinPeeters) (voor het design), [Senne](https://twitter.com/sennetorfs) (voor de frontend en het verzinnen van extra functionaliteiten) en [Sam](https://twitter.com/samboonen) (backend, frontend, bot, ...) vermelden omdat ze er mee voor zorgden dat we dit kleine projectje toch al functioneel hebben.