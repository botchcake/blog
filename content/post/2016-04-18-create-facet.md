---
categories:
  - facets
date: 2016-04-18 13:10:00
title: Create a facet from scratch
url: /2016/04/18/create-facet/
---


The administration UI of facets hasn't changed a lot between alpha-1 and [last week's alpha-2 release](http://tech.dichtlog.nl/facets/2016/04/12/facets-alpha-2.html), and we don't forsee that changing soon.
This demo uses Search API alpha-13 and facets alpha-2 and explains how to create a facet.

Before recording, I created 4 users and 24 nodes with [devel generate](https://www.drupal.org/project/devel).
This was otherwise a freshly installed Drupal 8.

<iframe width="450" height="325" src="https://www.youtube.com/embed/t5Ux6xDUWYw" frameborder="0" allowfullscreen></iframe>
- [https://youtu.be/t5Ux6xDUWYw](https://youtu.be/t5Ux6xDUWYw)

Make sure the right modules are installed, in this example we use the database server as a backend.
Installed modules in the video are: Search API, Database search and facets.

We start by creating a Search API server and index.
We also have to create a view based on that index.
Those steps can be skipped when using the DB defaults module.

After creating the view, it's time to create a facet.
Facets only work on fields that are indexed, by default that's only the language field.
We can add new fields into the index, but make sure you don't forget to reindex all items.