---
categories:
  - PHP
date: 2012-07-31 20:52:55
title: CodeIgniter email class
url: /2012/07/31/codeigniter-email-class/
---


De [emailclass van CodeIgniter](http://www.codeigniter.com/user_guide/libraries/email.html) werkt over het algemeen best goed.
Er is wel 1 ding waar je rekening mee moet houden als je een html-email wil versturen

De [user-guide](http://codeigniter.com/user_guide/) geeft geen duidelijke manier weer om het mailtype in te stellen maar aan het hand van het voorbeeld op die pagina zou je denken dat het zo gaat:

```php
<?php $this->email->mailtype('html'); ?>
```
Als het zo makkelijk zou zijn, dan zou het natuurlijk de moeite niet zijn om er een blogpost over te schrijven.
Het is blijkbaar zo dat dit niet werkt.
Als je bovenstaande code gebruikt dan krijg je: **Call to undefined method CI_Email::mailtype()**

Onder het hoofdstuk "Setting Email Preferences" vind je volgende voorbeeld code:

```php
<?php

$config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['wordwrap'] = TRUE;
$this->email->initialize($config);

```
Je zou dus denken dat je gewoon ``$config['mailtype'] = 'html'`` kan toevoegen.
Het probleem is dat de ``initizialize();`` method de email zelf wil versturen.
Dat was in mijn geval niet de bedoeling.
De oplossing? Eenvoudiger dan ik dacht!
In de map ``application/config`` maak je een nieuw bestand aan: ``email.php``. 
In dat bestand kan je gewoon een array aanmaken met initiele config voor de emailclass

```php
<?php

$config['mailtype'] = 'html';
$config['wordwrap'] = TRUE;
```
