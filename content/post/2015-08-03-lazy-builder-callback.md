---
categories:
  - PHP
date: 2015-08-03 08:25:55
title: Lazy builder callback
url: /2015/08/03/lazy-builder-callback/
---


This week, I've been working on a couple of issues in the [Drupal cacheability issue queue](https://www.drupal.org/project/issues/search/drupal?project_issue_followers=&status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=4&version%5B%5D=8.x&issue_tags_op=%3D&issue_tags=D8+cacheability%2C+D8+cache+tags).
In a couple of those issues, I've had the opportunity to work with *lazy builders*.
This is a really interesting new concept in drupal 8, and I'm going try to explain a little bit more about how this works here.

We're going to do this by creating a block that will display a random icecream flavor.

Now, there are 2 options to resolve this issue:
1. Make every page with the block on it uncacheable. This is horrible for performance but it'll work. 
  This is simple to do: ``$build['#cache]['max-age'] = 0;``
1. The second solution is introducing a placeholder and attaching a lazy builder to it.
  This is a little bit more complex but it's the best for cacheability because you can have the rest of the render array be cacheable and only have one part of it being uncacheable.

A lazy builder, is a piece callback function that gets executed every request, so that the surrounding code is can still be cached.
This is a way to make sure that all your pages can be cached and keep all the required complexity.
First of all, we need to implement a *callback* for the *lazy builder*.

```php
<?php

namespace Drupal\icecream;

class Icecream {

  /**
   * Returns a renderable array containing an ice cream flavour.
   *
   * @return array
   */
  public function lazyCallback() {
    $flavors = [
      'chocolate',
      'vanilla',
      'chocolate chip cookie dough',
      'grape',
      'straciatella',
      'rocky road',
    ];

    // This is never cacheable, because of the max_age 0.
    return [
      '#markup' => $flavors[array_rand($flavors)],
      '#cache' => ['max_age' => 0],
    ];
  }
}

```
So, we've created a new class that has one public method, the ```lazyCallback``` method.
This can be as complex as you want, as long as you're returning a [renderable array](https://www.drupal.org/developing/api/8/render/arrays).
In this example the renderable array is explicitly non-cacheable, but because this is just another render-array, it can have it's own set of cache-tags / cache-contexts.

If you want to have a different flavor based on every page, but it can be cached per url, the only change needed would be to change the ```#cache``` part of the returned render array.

```php
<?php

return [
  '#cache' => [
    'contexts' => ['url']
  ]
];
```

Now, here's the block that will use this callback:

```php
<?php

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // This placeholder is just a unique string, it has no other requirements
    // other than being injected in the places than where you want to have the
    // #lazy_builder take over.
    $placeholder = 'iceCream' . crc32('lazy icecream block');

    // Makes sure the placeholder will be replaced by an ice cream flavor by
    // attaching a lazy builder that will make the rest of this block cacheable.
    $build['#attached']['placeholders'][$placeholder] = [
      '#lazy_builder' => ['icecream:lazyCallback', []]
    ];

    // This is really intensive to calculate, so that's why we're caching the
    // entire block and having the lazy builder take care of the uncacheable
    // part of the block.
    $build['#markup'] = 'This is being generated with a placeholder.
    It is time for ' . $placeholder . ' ice cream. Hurray for ice cream!';


    // Returns the renderable array with attached placeholder.
    return $build;
  }
```

You can see in the code, that we've set ```#lazy_builder``` as an array containing a string and another array.
If you don't specify the second array, Drupal will give you an error with the following message: *\#lazy_builder property must have an array as a value, containing two values: the callback, and the arguments for the callback.*
The callback is set as ```'icecream:lazyCallback'```.
This works because we've defined the class in services.yml as ```icecream```

```yaml
## services.yml

services:
    icecream:
        class: Drupal\icecream\Icecream
```
These are all the pieces that you need to make a ```#lazy_builder``` callback.
You can use this solution every time you have a small portion of a page that's too complex to be cached.
Lazy builders make sure that the rest of the page is still cacheable, and that can help move quite some load off the server.

If you want to have a look at the complete code, you can check it out on my bitbucket: [https://bitbucket.org/botchcake/icecream-lazybuilder/src](https://bitbucket.org/botchcake/icecream-lazybuilder/src).

---

Update - note that the placeholders that are currently being created in the block aren't going to be needed once [https://www.drupal.org/node/2499157](https://www.drupal.org/node/2499157) lands.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/Borisson">@Borisson</a> Note that once <a href="https://t.co/KPJLqdUZKf">https://t.co/KPJLqdUZKf</a> lands, you won&#39;t have to deal with all that placeholder stuff anymore!</p>&mdash; Wim Leers (@wimleers) <a href="https://twitter.com/wimleers/status/628180356311740416">August 3, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/Borisson">@Borisson</a> Also, you can already just do <a href="https://twitter.com/hashtag/create_placeholder?src=hash">#create_placeholder</a> ⇒ TRUE. (The core issues you worked on are edge cases, it&#39;s not possible there.)</p>&mdash; Wim Leers (@wimleers) <a href="https://twitter.com/wimleers/status/628180578471419904">August 3, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
