---
layout: simple
title: About
permalink: /about/
toc: false
---

I'm is a web developer based in Belgium. 
I share my time working between [calibrate](https://www.calibrate.be/nl) and [codana](https://www.codana.be/nl).

For a few years, I was specialized in Drupal, and Drupal's [Search API](https://www.drupal.org/project/search_api) and [Facets](https://www.drupal.org/project/facets) modules in particular.
Nowadays I spend more time with Laravel and React, but also still work with Drupal sometimes.  
My current focus is working on config validation/recipes in Drupal Core in my open source time.

I try to spend my free time outdoors, by going hiking or cycling.

I've [spoken at a few conferences](/talks) around Europe.
I also helped organise, Drupal Dev Days in Ghent (2020; moved to 2022), as well as Drupal Camps (Ghent 2019 and 2024), as well as a festival in the past.
Of course I also help run some of our local users groups and I'm a board member of the belgian drupal association (dugbe vzw).

You can find more around the web, where you can usually find me as _borisson_.

<ul class="leading-loose clearfix">
  <li class="w-1/2 float-left"><a href="https://twitter.com/borisson">Twitter</a></li>
  <li class="w-1/2 float-left"><a href="https://drupal.community/@borisson_">Mastodon</a></li>
  <li class="w-1/2 float-left"><a href="https://github.com/borisson">GitHub</a></li>
  <li class="w-1/2 float-left"><a href="https://bitbucket.org/botchcake">BitBucket</a></li>
  <li class="w-1/2 float-left"><a href="https://www.drupal.org/u/borisson_">Drupal</a></li>
  <li class="w-1/2 float-left"><a href="https://www.linkedin.com/in/borisson">LinkedIn</a></li>
  <li class="w-1/2 float-left"><a href="http://www.last.fm/user/borisson_">Last.fm</a></li>
  <li class="w-1/2 float-left"><a href="https://www.facebook.com/borisson">Facebook</a></li>
  <li class="w-1/2 float-left"><a href="http://www.meetup.com/members/84918212/">Meetup</a></li>
  <li class="w-1/2 float-left"><a href="http://stackoverflow.com/users/1167934/botchcake">stackoverflow</a></li>
  <li class="w-1/2 float-left"><a href="http://www.slideshare.net/JorisVercammen">Slideshare</a></li>
</ul>

---

I've been to a couple of conferences over the last couple of years, you might have seen me at one of the following:

*2025:* Fosdem (Brussels, BE), Drupal Mountain Camp (Davos, CH), Drupal Dev Days (Leuven, BE)

*2024:* Fosdem (Brussels, BE), Drupal Global Sprint weekend (Ghent, BE), Laracon EU (Amsterdam, NL), Drupal Mountain Camp (Davos, CH), DrupalCamp Belgium (Ghent, BE),Drupal Dev Days (Burgas, BG), DrupalCon EU (Barcelona, ES), DrupalCamp Berlin (Berlin, DE)

*2023:* DrupalCon Lille (Lille, FR), Drupal Dev Days (Vienna AT)

*2022:* Drupal Dev Days Ghent (Ghent, BE), Drupal Mountain Camp (Davos, CH), DrupalCon Prague (Prague, CZ), Drupal Day PT (Lisbon, PT)

*2020:* DrupalJam (Online), Symfony World (Online)

*2019:* Drupal Dev Days (Cluj Napoca, RO), DrupalCon Europe (Amsterdam, NL)

*2018:* Fosdem (Brussels, BE), Drupal Dev Days (Lisbon, PT), Drupal Europe (Darmstadt, DE), Drupalcamp Belgium (Ghent, BE)

*2017:* Drupal Global Sprint Weekend (Ghent, BE), DrupalCamp Iceland (Reykjavik, IS), Drupalcamp Belgium (Antwerp, BE), DrupalCon (Vienna, AT)

*2016:* Drupal Global Sprint Weekend (Ghent, BE), DrupalCamp Spain (Granada, ES), DrupalJam (Utrecht, NL), Frontend United (Ghent, BE), Drupal Dev Days (Milan, IT), Drupalaton (Balatonalmadi, HU), DrupalCamp Belgium (Ghent, BE), DrupalCon (Dublin, IE), Iron camp (Prague, CZ)

*2015:* PHPBenelux Conference (Antwerp, BE), Fosdem (Brussels, BE), Drupal Global Sprint Weekend (Ghent, BE), Drupal Dev Days (Montpellier, FR), Drupalaton (Keszthely, HU), DrupalCon (Barcelona, SP), DrupalCamp Belgium (Leuven, BE), DrupalCamp Vienna (Vienna, AT)

*2014:* Drupal Global Sprint Weekend (Leuven, BE), Drupal Dev Days (Szeged, HU), DrupalCon (Amsterdam, NL), DrupalCamp Belgium (Ghent, BE).

*2013:* PHPBenelux Conference (Antwerp, BE), Fosdem (Brussels, BE), DrupalCamp Belgium (Leuven, BE), DrupalCon (Prague, CZ)

*2007:* Barcamp (Brussels, BE)

*2006:* Barcamp (Brussels, BE)

*I'm not including all the user group meetups I've been to, my <a href="http://www.meetup.com/members/84918212/">Meetup</a> profile is a better log to those.*

