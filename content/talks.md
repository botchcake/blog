---
layout: simple
title: Talks
toc: false
permalink: /talks/
---

In september of 2015, I got the opportunity to help <a href="https://twitter.com/nick_vh">Nick Veenhof</a> present at the Belgian DrupalCamp in Leuven about Search API.
Since that first time, I presented a few more times, here's an overview of all of them.

## 2025
- _Drupal Mountain Camp 2025_: Successfully contributing on drupal.org

### 2024
- _Drupal Mountain Camp 2024_: Configuration Validation in Drupal Core
- _DrupalCamp Ghent 2024_: Configuration Validation in Drupal Core
- _PHP-Leuven Meetup_: Monoliths don't need to be painful (not about drupal)
- _DrupalCon 2024_: Configuration Validation in Drupal Core
- _DrupalCamp Berlin 2024_: Configuration Validation in Drupal Core

### 2023
- _Drupal Mountain Camp 2023_: Test Driven Development workshop: Building a wordle clone in Drupal

### 2022
- _Drupal Dev Days Ghent 2022_: Building Google like search experiences without busting the bank (<a href="https://drupalcamp.be/en/drupal-dev-days-2022/session/building-google-search-experiences-without-busting-bank">more info</a>)
- _DrupalCon Prague 2022_: Building Google like search experiences without busting the bank
- _Drupal Day Lisbon 2022_: Building Google like search experiences without busting the bank

### 2018
- _Drupal Tech talks Netherlands 2018_: Facets under the hood (<a href="https://www.meetup.com/drupalnl/events/248887644/">more info</a>)
- _Drupal Europe 2018_: Bringing the Search API to drupal core (<a href="https://www.drupaleurope.org/session/bringing-search-api-drupal-core.html">more info</a>)
    * This was a core conversation, not a typical presentation, see more info <a href="https://www.drupal.org/project/ideas/issues/2999664">in Drupal's idea queue</a>
- _DrupalCamp Ghent 2018_: Search API Best practices (<a href="https://drupalcamp.be/en/drupalcamp-ghent-2018/session/search-api-best-practices">more info</a>)

### 2017
- _Drupal night (Zaventem)_: Search API ecosystem in drupal 8
- _DrupalCamp Iceland 2017_: Search API ecosystem in drupal 8
- _DrupalCamp Antwerp 2017_: Search API ecosystem in drupal 8

### 2016
- _DUGBE_: Search API ecosysteem in drupal 8 (<a href="http://meetup.com/DUG-BE/events/230291730/">more info</a>)
- _DrupalCamp Ghent_: Search API ecosystem in drupal 8 (<a href="http://gent2016.drupalcamp.be/gent_2016/sessions/search-api-ecosystem-drupal-8.html">more info</a>)
- _DrupalCon Dublin</i>: Search API ecosystem in drupal 8 (<a href="https://events.drupal.org/dublin2016/sessions/search-api-ecosystem-drupal-8">more info</a>)
- _DUGBE_: Cacheability metadata in drupal 8 (<a href="http://www.meetup.com/DUG-BE/events/232748594/">more info</a>)
- _Drupal Ironcamp_: Search API ecosystem in drupal 8
-  _Dinner with Drupal_: Search API ecosystem in drupal 8 (<a href="http://www.meetup.com/Drupal-Amsterdam/events/235439649/">more info</a>)

### 2015
- _DrupalCamp Leuven_: State of Search, solr and facets in drupal 8. (with <a href="http://nickveenhof.be/">Nick veenhof</a>) (<a href="http://leuven2015.drupalcamp.be/leuven_2015/leuven2015.drupalcamp.be/sessions/state-search-solr-and-facets-drupal-8.html">more info</a>)
- _DrupalCamp Leuven_: Q&A drupal 8. (with <a href="http://nickveenhof.be/">Nick veenhof</a>, <a href="http://wimleers.com/">Wim Leers</a>, <a href="http://www.frenssen.be/">Pieter Frenssen</a>, <a href="http://realize.be/">Kristof De Jaeger</a>, <a href="https://twitter.com/laurii1">Lauri Eskola</a>) (<a href="http://leuven2015.drupalcamp.be/leuven_2015/leuven2015.drupalcamp.be/sessions/qa-drupal-8.html">more info</a>)
